variable "region" {
  type = string

}
variable "availability_zone" {
  type = list(any)

}
variable "vpc_cidr" {
  type = string

}

variable "secure_cidr" {
  type = list(any)

}

variable "private_cidr" {
  type = list(any)

}

variable "public_cidr" {
  type = list(any)

}

variable "number_resources" {
  default = 2
}
